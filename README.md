Terraform Module for deploying an AKS cluster
=====================================

[![Opstree Solutions][opstree_avatar]][opstree_homepage]

[Opstree Solutions][opstree_homepage] 

  [opstree_homepage]: https://opstree.github.io/
  [opstree_avatar]: https://img.cloudposse.com/150x150/https://github.com/opstree.png


This terraform module will create a AKS Cluster.
This projecct is a part of opstree's ot-azure initiative for terraform modules.


Usage
------


```hcl
module "aks_subnet" {
  source         = "./modules/subnet"
  subnet_name    = var.aks_subnet_name
  subnet_rg      = var.rg_name
  vnet_name      = var.vnet_name
  address_prefix = var.aks_address_prefix
}

module "buildpiper_subnet" {
  source         = "./modules/subnet"
  subnet_name    = var.buildpiper_subnet_name
  subnet_rg      = var.rg_name
  vnet_name      = var.vnet_name
  address_prefix = var.buildpiper_address_prefix
}

module "buildpiper_nsg" {
  source        = "./modules/nsg"
  nsg_name      = var.buildpiper_nsg_name
  nsg_location  = var.rg_location
  nsg_rg_name   = var.rg_name
  security_rule = var.buildpiper_nsg_security_rule
  tagMap        = var.tagMap
}

module "buildpiper_vm" {
  source = "./modules/vm"
  nic_ip_configuration_subnet_id = module.buildpiper_subnet.subnet_id
  name                    = var.buildpiper_vm_name
  resource_group_name     = var.rg_name
  location                = var.rg_location
  vm_size                 = var.buildpiper_vm_size
  storage_image_reference = var.buildpiper_vm_storage_image_reference
  storage_os_disk         = var.buildpiper_vm_storage_os_disk
  os_profile              = var.buildpiper_vm_os_profile
  os_profile_linux_config = var.buildpiper_vm_os_profile_linux_config

  tagMap = var.tagMap
}

module "aks" {
  source              = "./modules/aks"
  aks_name            = var.aks_name
  location            = var.rg_location
  rg_name             = var.rg_name
  dns_prefix          = var.aks_dns_prefix
  vm_size             = var.aks_vm_size
  availability_zones  = var.aks_availability_zones
  node_type           = var.aks_node_type
  max_count           = var.aks_max_count
  min_count           = var.aks_min_count
  node_count          = var.aks_min_count
  vnet_subnet_id      = module.aks_subnet.subnet_id
  tagMap              = var.tagMap
}

module "agw" {
  source                      = "./modules/applicationgw"
  agw_name                    = var.agw_name
  agw_resource_group_name     = var.rg_name
  agw_resource_group_location = var.rg_location
  agw_vnet_name               = var.vnet_name
  request_routing_rule        = var.agw_request_routing_rule
  http_listener               = var.agw_http_listener
  backend_http_settings       = var.agw_backend_http_settings
  backend_address_pool        = var.agw_backend_address_pool
  frontend_port               = var.agw_frontend_port
  sku                         = var.agw_sku
  agw_address_prefix          = var.agw_address_prefix
  agw_security_rule           = var.agw_security_rule

  tagMap = var.tagMap
}

```

```sh
$   cat output.tf
/*-------------------------------------------------------*/
















/*-------------------------------------------------------*/
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| subnet_name | Name of the subnet. | string | null | yes |
| subnet_rg | RG of the subnet. | string | myrg | yes |
| vnet_name | Name of the vnet. | string | null | yes |
| address_prefix | Subnet address space  | list(string) | null | no |
| nsg_name | Name of SG | string | null | yes |
| nsg_location | Name of SG Location | (string) | null | yes |
| nsg_rg_name | Name of SG Resource Group | string | null | yes |
| security_rule | mention the security rules | list | null | no |
| nic_ip_configuration_subnet_id | Network Interface ID's which should be associated with the Virtual        Machine.| string | null | yes |
| name | name of the Virtual Machine. | string | null | yes |
| vm_size |  size of the Virtual Machine | string | null | yes |
| storage_image_reference | A storage image reference block. | any | null | no |
| storage_os_disk | A storage os disk block | any | null | yes |
| os_profile | Required, when a Linux machine | any | null | yes |
| os_profile_linux_config | Required, when a Linux machine | any | null | yes |
| aks_name | Name of the cluster | string | null | yes |
| dns_prefix | DNS prefix for the cluster | string | null | no |
| availability_zones | Availability Zones which the Node Pool should be spread | number | null | no |
| node_type | Type of node pool | string | null | no |
| max_count | Max number of nodes in te pool | number | null | yes |
| min_count | Min number of nodes in te pool | number | null | yes |
| node_count | Node count in the pool | number | null | yes |
| vnet_subnet_id | Subnet to lauch the nodes in | string | null | no |

